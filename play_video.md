---
layout: default
title: Screencast
---

Screencast
=============================

How to import large amounts of data

<video width="100%" ddheight="240" controls>
<source src="https://pyspread.gitlab.io/media/videos/pyspread_podcast_1.mp4" type="video/mp4">
Your browser does not support the video tag.
</video>