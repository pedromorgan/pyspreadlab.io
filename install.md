---
layout: default
title: Install
meta_description:  >
  Download and install for Linux and Windows
---

# Download & Install

*pyspread* works on Linux, Posix platforms and Windows, on which
[PyQt5](https://www.riverbankcomputing.com/software/pyqt/intro){:target="blank"}
is available. While there have been reports that *pyspread* can be used on
OS X, OS X is currently unsupported ([can you help?](contribute.html)).

**Select version**
- *pyspread* **v1** - [See install v1.x](install_v1.html)
- *pyspread* **v2** - follows below

## Install pyspread v1.99.1+

### From pypi

```bash
pip install pyspread
```

### From git

It is assumed that python3 and git are installed.

**Get sources and enter dir**
```bash
git clone https://gitlab.com/pyspread/pyspread.git
# or
git clone git@gitlab.com:pyspread/pyspread.git

# then
cd pyspread

```

**Install Dependencies**

_Linux_

```bash
pip3 install -r requirements.txt
```

_Windows_

```
pip install -r requirements.txt
```

**Launch app**

_Linux_

```bash
pyspread
```

_Windows_

```
pyspread.bat
```



