---
layout: default
title: Welcome to pyspread
meta_description:  >
  pyspread is a non-traditional spreadsheet that is based on and written
  in the programming language Python. It expects Python expressions in its
  grid cells, which makes a spreadsheet specific language obsolete.

---

Welcome to pyspread
===================


*pyspread* is a non-traditional spreadsheet that is based on and written in the programming language Python.
The goal of *pyspread* is to be the most pythonic spreadsheet.

*pyspread* expects Python expressions in its grid cells, which makes a spreadsheet specific language obsolete. Each cell returns a
Python object that can be accessed from other cells. These objects can represent anything including lists or matrices.

*pyspread* is free software. It is released under the GPL v3 licence.

The latest stable release v1.1.3 of *pyspread* runs on Python 2.7.x.

A Python 3 compatible version that runs on Python 3.6+ is available as beta release v1.99.1 ([source code](https://gitlab.com/pyspread/pyspread){:target="blank"}).

![Pyspread example]({{ site.baseurl }} /images/screenshot_sinus_large.png){:class="img-fluid"}


## Features

* Cells accept Python code and return Python objects
* Access to Python modules from within cells including e.g. NumPy
* Cells may display text, markup, images or charts
* Imports CSV
* Exports CSV, PDF, SVG
* git-able pysu save file format
* blake2b based save file signatures that prevent foreign code execution
* Spell checker*

Notes: * requires optional dependencies

## Target User Group

Directly using Python code in a grid is a core feature of *pyspread*. The target user group has
experience with or wants to learn the programming language Python:

- Peter regularly documents layout planning projects. He already has a large base of Python code for doing
  calculations in this task. Peter has to provide project documentation in pdf files with a consistant layout. In the documentation, he
  has to visualize building layouts and calculation charts.
- Clara is a research engineer and wants to quickly create presentations and
  posters with a consistent layout and publication ready figures for her publications. She is proficient with
  Python and uses it for her scientific analyses.

Not part of the target user group are Donna and Jack:

- Donna is looking for a free replacement for Ms. Excel. She does not know any programming language.
- Jack does computation intensive data analysis that may take hours to compute. He is looking for a visually interactive data mining tool.

This does not mean that Donna or Jack cannot work with *pyspread*. However, Donna might find the learning
curve for using Python code in cells too steep. Jack on the other hand might be disappointed because his
long running tasks are likely to lock up pyspread.

## Contact


For user questions or user feedback please use the
[pyspread community board](https://gitter.im/pyspread/community){:target="blank"}
on gitter.

For contributions, patches, development discussions and ideas please create an issue using the *pyspread* issue tracker
