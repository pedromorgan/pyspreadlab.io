---
layout: default
title: FAQ
---

# FAQ

## What kind of code can a cell contain ?

Note: The following text applies for pyspread >1.99.0

A cell can contain arbitrary lines of Python code. However, the last non-empty line must contain a Python expression such as a list comprehension or a generator expression. However, the last line cannot contain Python statements such as

```python
for i in xrange(10): pass
```

Variables that are defined by the lines before the last line are treated locally, i.e. they are not accessible from outside the cell.

The expression in the last line may be assigned to one global variable. That means
```python
a = 1
```
is valid and creates a global variable `a`. However,
```python
a = b = 1
```
is invalid in the last line of code.

## What are the boundaries for the number of rows, columns and tables?

The number of rows is limited to 1 000 000, the number of columns to 100 000 and the number of tables to 100.

These values can be adjusted in the file settings.py. Note that pyspread may become unresponsive for large grids.
