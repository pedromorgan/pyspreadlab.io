---
layout: default
title: Install v1.x
---

# Install pyspread v1.x


*pyspread* works on Linux and other GTK platforms as well as on Windows. While
there have been reports that it can be used on OS X as well, OS X is currently unsupported.

The following packages/software need to be installed:

- Python (>=2.7, <3.0)
- numpy (>=1.1.0)
- wxPython (>=2.8.10.1, Unicode version required)
- matplotlib (>=1.1.1)
- pycairo (>=1.8.8)
- python-gnupg (>=0.3.0, for opening own files without approval)

The following optional dependencies improve user experience:

- xlrd (>=0.9.2, for loading Excel files)
- xlwt (>=0.9.2, for saving Excel files)
- jedi (>=0.8.0, for tab completion and context help in the entry line)
- pyrsvg (>=2.32, for displaying SVG files in cells)
- pyenchant (>=1.1, for spell checking)
- Python bindings for libvlc (with libvlc.so.5, for video playback)
- basemap (>=1.0.7, for the weather example pys file)

## Installation

### From Source

- Ensure that all dependencies are installed.
- Unpack the source tarball (gpg sig).
- cd to the extraction directory
- Type in: `python setup.py install`

## Linux

The following Linux distributions provide pyspread as a package:

- ![](images/arch-logo-small.png) Arch - [pyspread v1.1.3](https://aur.archlinux.org/packages/pyspread/)
- ![](images/debian-logo-small.png) Debian - [pyspread v1.1.3](https://packages.debian.org/de/sid/pyspread)
- ![](images/fedora-logo-small.png) Fedora - [pyspread v1.1.2](https://fedora.pkgs.org/28/rpm-sphere/pyspread-1.1.2-4.1.noarch.rpm.html)
- ![](images/mageia-logo-small.png) Mageia - [pyspread v1.1](https://madb.mageia.org/package/show/name/pyspread)
- ![](images/nixos-hex.png) NixOS - [pyspread v1.1.3](https://nixos.org/nixos/packages.html#pyspread)
- ![](images/slackware-logo-small.png) Slackware - [pyspread v1.1.3](https://slackbuilds.org/repository/14.2/office/pyspread/)
- ![](images/ubuntu-logo-small.png) Ubuntu - [pyspread v1.1.3](https://packages.ubuntu.com/eoan/pyspread)

For these distributions, the easiest way is to install
*pyspread* from the repositories. The source distribution can also be used
for example if the packaged version is outdated.

## Windows with installer
For version 1.1, a Windows 32 bit installer is available (Windows XP or later). Note that
the installer does not comprise all libraries that are used in the examples, i.e. maps and videos in the
grid will not work out of the box.

- Download and install gnupg ([Gpg4win](https://www.gpg4win.org/), supports 32&64bit). Note that version 2.2.6
  works and  version 3.x does not. If you encounter issues please downgrade to 2.2.6.
- Download the [pyspread installer](https://github.com/manns/pyspread/releases/download/v1.1.1/setup_pyspread_1.1.1.exe)
  (32MB download, [gpg sig](https://github.com/manns/pyspread/releases/download/v1.1.1/setup_pyspread_1.1.1.exe.sig)).
- Run the `setup.py` file.

## Windows with manual installation

If you want to customize a Windows installation or if you require 64 bit then install *pyspread* manually. Please
follow these instructions. Links point at sites where both 32 bit and to 64 bit versions are provided.

- Download and install Python, numpy and matplotlib. You can find 32bit and 64bit packages of WinPython
  [here](http://winpython.sourceforge.net/). Make  sure to get the Python2 version. If WinPython is used then
  the WinPython control panel is recommended  for all installations.
- Download and install Cairo and pycairo. Working binaries are
  available [here](http://www.lfd.uci.edu/~gohlke/pythonlibs). Note that Cairo has to be installed
  by manually copying it. Make sure to read the installation instructions.
- Download and install [wxPython 2.8.x or 3.x.](http://www.wxpython.org/download.php#stable)
- Download and install gnupg [Gpg4win](https://www.gpg4win.org/) (supports 32&64bit)
- Download and install [python-gnupg](http://pythonhosted.org/python-gnupg/#download):
  - For 32 bit systems, an exe installer (`python-gnupg-0.3.4.win32.exe`) is provided on the linked page.
  - For 64 bit systems, the python-gnupg tarball (`python-gnupg-0.3.4.tar.gz`) has to be extracted (e.g. with 7zip)
- Download and install the source distribution of *pyspread*
- Start pyspread by double-clicking on the file `pyspread.bat` in the extraction directory.
