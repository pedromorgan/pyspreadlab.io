---
layout: default
title: README.md
---

pyspread.gitlab.io
=======================

- This is the repos for the website at [https://pyspread.gitlab.io](https://pyspread.gitlab.io)
- It is [Jekyll](https://jekyllrb.com/) powered and create the site with `.gitlab-ci.yml`

## Overview

To get started..

- install jekyll
- execute `./run_dev.sh` to build locally

A quick rundown..

- jekyll config is in the `_config.yml` file
- The site layout is in the `_layouts/default.html` file
  which includes the `_includes/navbar/.html`
- The navigation are stored in the
   - `_data/main_nav.json` for main site
   - and `_data/manual_nav.json` for the manual sections
   - see below for nav items format
- Using utf8 symbols for simple icons eg `&#128213;`
- All paths are relative (see frontmatter below)

### front matter

For non manual pages frontmatter is simply
```yaml
---
layout: default
title: Charts
---
```

For manual pages which is in a subdirectory, the frontmatter is
```yaml
---
layout: default
title: Charts
section: manual
parent: ../
---
```
Where `parent` is the path to the root directory and `section: manual`

### nav items

basic requirments are
```json
{
"title": "Nav Title",
"url": "page.html"
}

```

`icon:` is utf8.html and `target:` opens up a new browser window
```json
{
"title": "Issues",
"url": "https://gitlab.com/pyspread/pyspread/issues",
"icon": "&#9636;",
"target": "gitlab"
}
```

### Syntax features

- pyspread is with italics ie `*pyspread*`

- h4 and h5 are used in the menus eg `##### -> Print`

- menus are highlighted with code wrapped in bold

```
**`File -> New`**
```

