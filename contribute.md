---
layout: default
title: Contribute
---


Contribute
======================


## Roadmap

 - Read the [Issue board](https://gitlab.com/pyspread/pyspread/-/boards/1361261)

*Pyspread* **1.x** only works with **Python 2.x**. Development concentrates on fixing bugs
and maintaining compatibility. Its source code currently is located in the master branch.

*Pyspread* **2.x** will be **Python3** compatible and is under
heavy development but not yet ready for production use. Its
source code currently is located in the python3 branch. New
features should be developed against pyspread 2.x.

The switch to Python3 goes along with changing the GUI toolkit
from wxPython to PyQt5. This change requires a major rewrite of the GUI
part of pyspread. For an overview of current and future tasks for realizing
pyspread 2.0, please refer to the to do list.


## How to Help

Thank you for your interest in improving *Pyspread*. *Pyspread* is
non-commercial. It lives from your contributions. Do not hesitate to help with
small things, e.g. stating a feature wish in the issue tracker.

## Bug Reports

For bug reports, please use the issue tracker.


## Patches


For patches / code contributions,

- fork the master branch and
- create a merge request that
- you refer to in an issue.

Please try solving only one issue or realize one feature per branch.